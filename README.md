# 410 on archived content
Returns http code 410 on an archived content

## Description
When an anonymous user requests an unpublished content which has the moderation state "archived", this event subscriber returns a response with an http code 410 (Gone). The advantage of this is that it advises search engines to remove the page from their index.

## Installation
Install the module  
(https://www.drupal.org/docs/extending-drupal/installing-modules).

## Configuration
You can choose the moderation state on the configuration page:  
Admin >> Config >> System >> Archived 410

## Dependency
The core module content moderation  
https://www.drupal.org/docs/8/core/modules/content-moderation/overview

## Sponsor
* State of Geneva - https://www.ge.ch

## Maintainers
* Mickaël Silvert - https://www.drupal.org/user/yepa
