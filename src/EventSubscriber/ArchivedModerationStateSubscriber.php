<?php

namespace Drupal\archived_410\EventSubscriber;

use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\BareHtmlPageRendererInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Exception\GoneHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class ArchivedModerationStateSubscriber.
 * When an anonymous user requests an unpublished content which has the
 * moderation state "archived", this event subscriber returns a response with
 * an http code 410 (Gone).
 *
 * @package Drupal\archived_410
 */
class ArchivedModerationStateSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The moderation information service.
   *
   * @var \Drupal\content_moderation\ModerationInformationInterface
   */
  protected $moderationInfo;

  /**
   * The bare HTML page renderer.
   *
   * @var \Drupal\Core\Render\BareHtmlPageRendererInterface
   */
  protected $bareHtmlPageRenderer;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\content_moderation\ModerationInformationInterface $moderationInfo
   *   Moderation information service.
   * @param \Drupal\Core\Render\BareHtmlPageRendererInterface $bare_html_page_renderer
   *   The bare HTML page renderer.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translation
   *   The translation service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(AccountInterface $current_user, EntityTypeManagerInterface $entity_type_manager, ModerationInformationInterface $moderationInfo, BareHtmlPageRendererInterface $bare_html_page_renderer, TranslationInterface $translation, ConfigFactoryInterface $config_factory) {
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
    $this->moderationInfo = $moderationInfo;
    $this->bareHtmlPageRenderer = $bare_html_page_renderer;
    $this->stringTranslation = $translation;
    $this->configFactory = $config_factory;
  }

  /**
   * This method is called whenever the kernel.request event is
   * dispatched.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function onRequestArchivedContent(GetResponseEvent $event) {
    $request = $event->getRequest();

    // If we've got an exception, nothing to do here.
    if ($request->get('exception') != NULL) {
      return;
    }

    // Check if user is anonymous.
    if ($this->currentUser->isAnonymous()) {

      // Get route name.
      $routeName = '';
      try {
        $routeName = Url::fromUserInput($request->getRequestUri())
          ->getInternalPath();
      }
      catch (\UnexpectedValueException $exception) {
        // Avoid to throw an exception for all externals requests.
      }

      // Load the related node.
      $parts = explode('/', $routeName);
      if (isset($parts[0]) && $parts[0] == 'node' && isset($parts[1])) {
        $node = $this->entityTypeManager->getStorage($parts[0])
          ->load(intval($parts[1]));

        // Check if moderation state is equal to archived.
        if (($node instanceof EntityPublishedInterface)
          && !$node->isPublished()
          && $this->moderationInfo->isModeratedEntity($node)
          && $node->get('moderation_state')->getString() == $this->configFactory->get('archived_410.settings')->get('moderation_state')) {

          // Throw a 410 error.
          throw new GoneHttpException('Resource gone!');
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // This event should be done before the node check access,
    // so the value should be greater or equal to 34.
    $events[KernelEvents::REQUEST][] = ['onRequestArchivedContent', 34];

    return $events;
  }

}
