<?php

namespace Drupal\archived_410\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'archived_410_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['archived_410.settings'];
  }

  /**
   * @TODO change the textfield to a select with the moderation states list
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['moderation_state'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Moderation State'),
      '#default_value' => $this->config('archived_410.settings')->get('moderation_state'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('archived_410.settings')
      ->set('moderation_state', $form_state->getValue('moderation_state'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
